/// <reference types="Cypress" />

import HomePage from '../../support/pageObjects/HomePage'
import ProductPage from '../../support/pageObjects/ProductPage'
import CheckOutPage from '../../support/pageObjects/CheckOutPage'

describe('Data File and Page Object', function() {
    
    before(function() {
        // runs once before all tests in the block
        cy.fixture('example').then(function(data){
            this.data=data  
        })
    })

    it('Loading from data file and call page objects created',function() {

        /// Home page Products ///
        
            // Object instance of the page object class - HomePage
            const homePage = new HomePage()

            // It could be hardcode in the cypress.json file
            cy.visit(Cypress.env("url"))

            // Call the function getEntrepreneur from HomePage class to go to the shop page
            homePage.getShopTab().click()

            
        /// Page Shop Products ///

            // Object instance of the page object class - ProductPage
            const productPage = new ProductPage()

            // Adding one product
            cy.selectProduct('iphone X') // For one or more product you can execute many times depends on the products list

            // Call the function getCheckout from ProductPage class to proceed checkout
            productPage.getCheckout().click()

        
        /// Check out Page ///

            // Object instance of the page object class - CheckOutPage
            const checkOutPage = new CheckOutPage()

            // Validate at least exist one item
            cy.get('h3 strong').should('have.length','1')

            // Return to the shop page
            checkOutPage.getContinueShoppingButton().click()
    })
})