// <reference types="Cypress" />
describe('Second test', function () {

    it('Let go to buy', () => {
    // Create your test here
    cy.visit('https://rahulshettyacademy.com/seleniumPractise/#/')
    cy.get('.search-keyword').type('ca')
    cy.wait(2000)
    //Iteration
    cy.get('.products').find('.product').each(($el, index, $list) => {
        const VegName=$el.find('h4.product-name').text()
        if (VegName.includes('Cashews'))
        {
            $el.find('button').click()
        }
    })
    cy.get('.cart-icon > img').click()
    cy.contains('PROCEED TO CHECKOUT').click()
    cy.contains('Place Order').click()

    });
});