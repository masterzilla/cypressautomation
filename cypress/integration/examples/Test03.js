/// <reference types="Cypress" />
describe('Second test', function () {

    it('Let go to buy', () => {
    // Create your test here
    cy.visit('https://rahulshettyacademy.com/AutomationPractice/')

    //Checkboxes
    cy.get('#checkBoxOption1').check().should('be.checked').and('have.value','option1')
        //(select check by #tagname).(Check instead of click).(Check the behaviour).(check the value)
    cy.get('#checkBoxOption1').uncheck().should('not.be.checked') //uncheck a checkbox
    //Multiple checkboxes
    cy.get('input[type="checkbox"]').check(['option2','option3'])

    //Static Dropdow
    cy.get('select').select('option2').should('have.value','option2')
        //two first parenthesis took the option and .should evaluate a property
    
    //Dynamic dropdowns
    cy.get('#autocomplete').type('ind')
    cy.get('.ui-menu-item div').each(($el, index, $list) => {
        if ($el.text()==='India')
        {
            $el.click()
        }
    })
    //Assertion of the value selected from the dynamic dropdown
    cy.get('#autocomplete').should('have.value','India')

    //Hidden and showned element
    cy.get('#displayed-text').should('be.visible')
    cy.get('#hide-textbox').click()
    cy.get('#displayed-text').should('not.be.visible')
    cy.get('#show-textbox').click()
    cy.get('#displayed-text').should('be.visible')

    //Radio button selection
    cy.get('[value="radio2"]').check().should('be.checked')
    cy.get('[value="radio3"]').check().should('be.checked')
    cy.get('[value="radio1"]').check().should('be.checked')

    });
});