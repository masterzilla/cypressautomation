/// <reference types="Cypress" />
describe('Second test', function () {

    it('Let go to buy', () => {
    // Create your test here
    //cy.visit('https://rahulshettyacademy.com/AutomationPractice/')
    cy.visit('http://qaclickacademy.com/practice.php')

    //Remove target to open a child tab
    cy.get('#opentab')
        .invoke('removeAttr','target')
        .click()
    //Validate the correct page
    cy.url().should('include','rahulshettyacademy')

    //Back to father page after validate the correct page
    cy.go('back')
    });
});