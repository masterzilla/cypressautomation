/// <reference types="Cypress" />

import HomePage from '../../support/pageObjects/HomePage'

describe('Full form apply Data File, Faker library and Page Object', function() {
    
    before(function() {
        // runs once before all tests in the block
        cy.fixture('example').then(function(data){
            this.data=data  
        })
    })

    it('Loading from data file and call page objects created',function() {

        /// Home page Products ///
        
            // Object instance of the page object class - HomePage
            const homePage = new HomePage()

            // The URL could be hardcode in the cypress.json file
            cy.visit(Cypress.env("url"))
            
            // Call the function getEditName from HomePage class to fill up the field Name
            homePage.getEditName().type(this.data.name)

            // Call the function getEditEmail from HomePage class to fill up the field Email
            homePage.getEditEmail().type(this.data.email)

            // Call the function getEditPwd from HomePage class to fill up the field Password
            homePage.getEditPwd().type(this.data.password)

            // Call the function getCheckLoveIce from HomePage class to check a paramenter
            homePage.getCheckLoveIce().click()

            // Call the function getGender from HomePage class to select a gender
            homePage.getGender().select(this.data.gender)

            // Call the function getRadioEmploymentStatus from HomePage class to check a parameter
            homePage.getRadioEmploymentStatus().click()

            // Set a BirthDay
            cy.get(':nth-child(8) > .form-control').invoke('val','01/06/1992')

            // Call the function getTowWayDatainding from HomePage class to validate both fields have the same value in the field name
            homePage.getTowWayDatainding().should('have.value',this.data.name)
            
            // Validate the minlength propertie is complied from the function getEditBox AND also works to validate any HTML propertie (value)
            homePage.getEditName().should('have.attr','minlength','2')

            // Call the function getEntrepreneur from HomePage class to validate the radio button is disabled
            homePage.getEntrepreneur().should('be.disabled')

            //Configuration to load a specific field on delivery location
            Cypress.config('defaultCommandTimeout', 8000)

            // Call the function getSubmit from HomePage class to send customer information
            homePage.getSubmit().click()

            // Compare the alert message after complete the buy process
            cy.get('.alert').then(function(element)
            {
                const submitText = element.text()
                expect(submitText.includes("submitted")).to.be.true
            })
            
    })
})