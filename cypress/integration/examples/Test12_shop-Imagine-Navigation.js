/// <reference types="Cypress" />

import HomePage from '../../support/pageObjects/HomePage'
import ProductPage from '../../support/pageObjects/ProductPage'

describe('Applying Page Object - Imagine navigation ', function() {
    
    it('Imagine navigation',function() {

        /// Home page Products ///
        
            // Object instance of the page object class - HomePage
            const homePage = new HomePage()

            // The URL could be hardcode in the cypress.json file
            cy.visit(Cypress.env("url"))

            // Call the function getShopTab from HomePage class to send customer information
            homePage.getShopTab().click()

        /// Shop Page Products ///

            // Object instance of the page object class - Page Shop
            const productPage = new ProductPage()
        
            cy.get('.carousel-indicators').each(($el, index, $list) => {
                if ($el.text('active'))
                {
                    productPage.getPreviousButton().click()
                }
            })

            // Click on the Next button
            var get = 0
            productPage.getNextButton().click()

            // Click on the Previous button
            productPage.getPreviousButton().click()

    })
})