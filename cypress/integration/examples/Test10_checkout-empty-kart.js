/// <reference types="Cypress" />

import HomePage from '../../support/pageObjects/HomePage'
import ProductPage from '../../support/pageObjects/ProductPage'

describe('Applying Page Object - Validate an empty car', function() {
    
    it('Going to shop page',function() {

        /// Home page Products ///
        
            // Object instance of the page object class - HomePage
            const homePage = new HomePage()

            // The URL could be hardcode in the cypress.json file
            cy.visit(Cypress.env("url"))

            // Call the function getShopTab from HomePage class to send customer information
            homePage.getShopTab().click()

        /// Shop Page Products ///

            // Object instance of the page object class - Page Shop
            const productPage = new ProductPage()
        
            // Call the function getCheckout from ProductPage class to proceed checkout
           productPage.getCheckout().click()

        /// Check out page ///

            // Validate empty car
            var sum=0
            cy.get('h3 strong').then(function(element){ //--------------------------------------->  // We have to resolve the promise with Cypress (get)
                const amount = element.text() // Get the column value                               // Javascript //
                var res = amount.split(" ") // Separate the value                                   // Javascript //
                var total = res[1].trim() // Keep with the amount and erase any special character   // Javascript //
                expect(Number(total)).to.equal(sum)                                                 // Javascript //
        })

    })
})