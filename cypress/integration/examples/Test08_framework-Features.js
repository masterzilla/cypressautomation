/// <reference types="Cypress" />

import HomePage from '../../support/pageObjects/HomePage'
import ProductPage from '../../support/pageObjects/ProductPage'
import CheckOutPage from '../../support/pageObjects/CheckOutPage'
import DeliveryPage from '../../support/pageObjects/DeliveryPage'

describe('Data File and Page Object', function() {
    
    before(function() {
        // runs once before all tests in the block
        cy.fixture('example').then(function(data){
            this.data=data  
        })
    })

    it('Loading from data file and call page objects created',function() {

        /// Home page Products ///
        
            // Object instance of the page object class - HomePage
            const homePage = new HomePage()

            // It could be hardcode in the cypress.json file
            cy.visit(Cypress.env("url"))
            
            // Call the function getEditBox from HomePage class to fill up the field Name
            homePage.getEditBox().type(this.data.name)
            
            // Call the function getGender from HomePage class to select a gender
            homePage.getGender().select(this.data.gender)

            // Call the function getTowWayDatainding from HomePage class to validate both fields have the same value in the field name
            homePage.getTowWayDatainding().should('have.value',this.data.name)
            
            // Validate the minlength propertie is complied from the function getEditBox AND also works to validate any HTML propertie (value)
            homePage.getEditBox().should('have.attr','minlength','2')

            // Call the function getEntrepreneur from HomePage class to validate the radio button is disabled
            homePage.getEntrepreneur().should('be.disabled')

            //Configuration to load a specific field on delivery location
            Cypress.config('defaultCommandTimeout', 8000)

            // Call the function getEntrepreneur from HomePage class to go to the shop page
            homePage.getShopTab().click()

            
        /// Page Shop Products ///

            // Object instance of the page object class - ProductPage
            const productPage = new ProductPage()

            // Select and execute the selectProduct function -> It was created on the commands.js file
            cy.selectProduct('iphone X') // For one or more product you can execute many times depends on the products list OR
            
            // You can create a pool of data as array in example.json file and instants to be executed as Array
            this.data.productName.forEach(function(element){
                cy.selectProduct(element)
            })

            // Call the function getCheckout from ProductPage class to proceed checkout
            productPage.getCheckout().click()

        
        /// Check out Page ///

            // Object instance of the page object class - CheckOutPage
            const checkOutPage = new CheckOutPage()

            //Validate items prices
            var sum=0
            cy.get('tr td:nth-child(4) strong').each(($e1, index, $list) => {               // Javascript //
                const amount = $e1.text() // Get the column value                           // Javascript //
                var res = amount.split(" ") // Separate the value                           // Javascript //
                res = res[1].trim() // Keep with the amount and erase any special character // Javascript //
                sum = Number(sum) + Number(res) // Collect the total price                  // Javascript //
            }).then(function(){ //------------------------------------------------------->  // We have to resolve the promise with Cypress (get)
                cy.log(sum)
            })

            cy.get('h3 strong').then(function(element){ //--------------------------------------->  // We have to resolve the promise with Cypress (get)
                const amount = element.text() // Get the column value                               // Javascript //
                var res = amount.split(" ") // Separate the value                                   // Javascript //
                var total = res[1].trim() // Keep with the amount and erase any special character   // Javascript //
                expect(Number(total)).to.equal(sum)                                                 // Javascript //
            })

            // Call the function getCheckOutButton from CheckOutPage class to the delivery location
            checkOutPage.getCheckOutButton().click()

        
        /// Delivery Page ///

            // Object instance of the page object class - CheckOutPage
            const deliveryPage = new DeliveryPage()

            // Search and select the delivery country
            cy.get('#country').type('India')
            cy.get('.suggestions > :nth-child(1) > li > a').click()

            // Call the function getAgreeTerms from DeliveryPage class to click on the check
            deliveryPage.getAgreeTerms().click({force:true})

            // Call the function getSubmitDelivery from DeliveryPage class to click on the Submit button
            deliveryPage.getSubmitDelivery().click()

            // Compare the alert message after complete the buy process
            cy.get('.alert').then(function(element)
            {
                const actualText = element.text()
                expect(actualText.includes("Success")).to.be.true
            })

    })
})