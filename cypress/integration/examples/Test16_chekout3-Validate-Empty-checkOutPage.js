/// <reference types="Cypress" />

import HomePage from '../../support/pageObjects/HomePage'
import ProductPage from '../../support/pageObjects/ProductPage'
import CheckOutPage from '../../support/pageObjects/CheckOutPage'

describe('Data File and Page Object', function() {
    
    before(function() {
        // runs once before all tests in the block
        cy.fixture('example').then(function(data){
            this.data=data  
        })
    })

    it('Loading from data file and call page objects created',function() {

        /// Home page Products ///
        
            // Object instance of the page object class - HomePage
            const homePage = new HomePage()

            // It could be hardcode in the cypress.json file
            cy.visit(Cypress.env("url"))

            // Call the function getEntrepreneur from HomePage class to go to the shop page
            homePage.getShopTab().click()

            
        /// Page Shop Products ///

            // Object instance of the page object class - ProductPage
            const productPage = new ProductPage()

            // Call the function getCheckout from ProductPage class to proceed checkout
            productPage.getCheckout().click()

        
        /// Check out Page ///

            // Object instance of the page object class - CheckOutPage
            const checkOutPage = new CheckOutPage()

            // Validate at least not exist any item
            cy.get('h3 strong').should('not.have.length')

            // Return to page products
            checkOutPage.getReturnPageShop().click()

        
        /// Page Shops Products - Adding products again /// 

            // Adding one product
            cy.selectProduct('iphone X') // For one or more product you can execute many times depends on the products list

            // Call the function getCheckout from ProductPage class to proceed checkout
            productPage.getCheckout().click()

        
        /// Check out Page - Again ///

            // Validate at least exist one item
            cy.get('h3 strong').should('have.length','1')

            // Call the function getCheckOutButtonAgain from CheckOutPage class to the delivery location
            checkOutPage.getCheckOutButtonAgain().click()

        
        /// Delivery Page ///

            // Call the function getSubmitDelivery from DeliveryPage class to click on the Submit button
            if (cy.get('#country').should('be.empty'))
            {
                cy.contains('Purchase').should('not.be.enabled')
            }
    })
})