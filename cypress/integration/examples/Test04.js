/// <reference types="Cypress" />
describe('Second test', function () {

    it('Let go to buy', () => {
    // Create your test here
    cy.visit('https://rahulshettyacademy.com/AutomationPractice/')
        
    cy.get('[value="Alert"]').click()
    //Assert message in a alert window
    cy.on('window:alert', (str) =>
    {
        //From Mocha 
        expect(str).to.equal('Hello , share this practice page and share your knowledge')
    })

    cy.get('#confirmbtn').click()
    //Assert message in a confirmation window
    cy.on('window:confirm', (str) =>
    {
        //From Mocha 
        expect(str).to.equal('Hello , Are you sure you want to confirm?')
    })

    });
});