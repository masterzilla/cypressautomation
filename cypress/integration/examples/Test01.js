/// <reference types="Cypress" />
describe('First test', function () {

    it('should open the main page', () => {
    // Create your test here
    cy.visit('https://rahulshettyacademy.com/seleniumPractise/#/')
    cy.get('.search-keyword').type('ca')
    cy.wait(2000)
    //cy.get('.product:visible').should('have.length',4)
    //Parent child chaining
    cy.get('.products').as('productLocator')
    cy.get('@productLocator').find('.product').should('have.length',4)
    cy.get(':nth-child(3) > .product-action > button').click()
    cy.get('@productLocator').find('.product').eq(2).contains('ADD TO CART').click()

    //Iteration
    cy.get('@productLocator').find('.product').each(($el, index, $list) => {
        const VegName=$el.find('h4.product-name').text()
        if (VegName.includes('Cashews'))
        {
            $el.find('button').click()
        }
    })

    //Assert if logo text is correct
    cy.get('.brand').should('have.text','GREENKART')
    });
});