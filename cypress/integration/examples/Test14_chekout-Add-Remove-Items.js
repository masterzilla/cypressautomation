/// <reference types="Cypress" />

import HomePage from '../../support/pageObjects/HomePage'
import ProductPage from '../../support/pageObjects/ProductPage'
import CheckOutPage from '../../support/pageObjects/CheckOutPage'
import DeliveryPage from '../../support/pageObjects/DeliveryPage'

describe('Data File and Page Object', function() {
    
    before(function() {
        // runs once before all tests in the block
        cy.fixture('example').then(function(data){
            this.data=data  
        })
    })

    it('Loading from data file and call page objects created',function() {

        /// Home page Products ///
        
            // Object instance of the page object class - HomePage
            const homePage = new HomePage()

            // It could be hardcode in the cypress.json file
            cy.visit(Cypress.env("url"))
            
            // Call the function getEntrepreneur from HomePage class to go to the shop page
            homePage.getShopTab().click()

            
        /// Page Shop Products ///

            // Object instance of the page object class - ProductPage
            const productPage = new ProductPage()

            // Select and execute the selectProduct function -> It was created on the commands.js file
            cy.selectProduct('iphone X') // For one or more product you can execute many times depends on the products list OR
            
            // You can create a pool of data as array in example.json file and instants to be executed as Array
            this.data.productName.forEach(function(element){
                cy.selectProduct(element)
            })

            // Call the function getCheckout from ProductPage class to proceed checkout
            productPage.getCheckout().click()

        
        /// Check out Page ///

            // Object instance of the page object class - CheckOutPage
            const checkOutPage = new CheckOutPage()

            // Validate at least exist one item
            cy.get('h3 strong').should('have.length','1')

            // Removing items 
            
            for(let n = 0; n < 3; n ++)
            {
                checkOutPage.getRemoveButton().click()
            }

            //cy.get('table.table-hover tbody tr td').should('not.contain',' ').find('button').click()

            /*cy.contains('Status').parent('tr').within(() => {
                // all searches are automatically rooted to the found tr element
                cy.get('td').eq(4).contains('button', 'Remove').click()
                /*cy.get('td').eq(2).contains('0')
                cy.get('td').eq(3).contains('Active')
                cy.get('td').eq(4).contains('Active')
                cy.get('td').eq(5).contains('button', 'Edit').click()
              })*/

            // Validate at least not exist any item
            cy.get('h3 strong').should('not.have.length')

    })
})