/// <reference types="Cypress" />

import HomePage from '../../support/pageObjects/HomePage'

describe('Applying Page Object - Counting items', function() {
    
    it('Counting items',function() {

        /// Home page Products ///
        
            // Object instance of the page object class - HomePage
            const homePage = new HomePage()

            // The URL could be hardcode in the cypress.json file
            cy.visit(Cypress.env("url"))

            // Call the function getShopTab from HomePage class to send customer information
            homePage.getShopTab().click()

        /// Shop Page Products ///

            // Counting number of items on a web page, using a number of items as parameter
            var showItem=4
            cy.get('app-card-list').find('app-card').its('length').should('eq',Number(showItem))
            cy.get('app-card-list').find('app-card').should('have.length',Number(showItem))
            cy.get('app-card-list').children().should('have.length',Number(showItem))
            cy.get('app-card-list').children().should(($h4) => {
                expect($h4).to.have.length(Number(showItem))
            })
    })
})