/// <reference types="Cypress" />

import HomePage from '../../support/pageObjects/HomePage'
import ProductPage from '../../support/pageObjects/ProductPage'

describe('Applying Page Object - Navigation options', function() {
    
    it('Navigation options',function() {

        /// Home page Products ///
        
            // Object instance of the page object class - HomePage
            const homePage = new HomePage()

            // The URL could be hardcode in the cypress.json file
            cy.visit(Cypress.env("url"))

            // Call the function getShopTab from HomePage class to send the shop page
            homePage.getShopTab().click()

        /// Shop Page Products /// Category 1

            // Object instance of the page object class - Page Shop
            const productPage = new ProductPage()
        
            // Call the function getCategory1 from ProductPage class to proceed check categoy 1 product
            productPage.getCategory1().click()

            // Call the function getShopTab from HomePage class to return the shop page products
            homePage.getShopTab().click()

        /// Shop Page Products /// Category 2

            // Call the function getCategory1 from ProductPage class to proceed check categoy 2 product
            productPage.getCategory2().click()

            // Call the function getShopTab from HomePage class to return the shop page products
            homePage.getShopTab().click()

        /// Shop Page Products /// Category 3

            // Call the function getCategory1 from ProductPage class to proceed check categoy 3 product
            productPage.getCategory3().click()

            // Call the function getShopTab from HomePage class to return the shop page products
            homePage.getShopTab().click()

    })
})