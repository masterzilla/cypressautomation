class DeliveryPage 
{
    getAgreeTerms()
    {
        return cy.get('#checkbox2')
    }

    getSubmitDelivery()
    {
        return cy.get('input[type="submit"]')
    }
}

export default DeliveryPage;