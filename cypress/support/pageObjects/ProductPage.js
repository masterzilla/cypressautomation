class ProductPage 
{
    getCheckout()
    {
        return cy.get('#navbarResponsive > .navbar-nav > .nav-item > .nav-link')
    }

    getCategory1()
    {
        return cy.get('.list-group > :nth-child(1)')
    }

    getCategory2()
    {
        return cy.get('.list-group > :nth-child(2)')
    }

    getCategory3()
    {
        return cy.get('.list-group > :nth-child(3)')
    }

    getPreviousButton()
    {
        return cy.get('.carousel-control-prev-icon')
    }

    getNextButton()
    {
        return cy.get('.carousel-control-next-icon')
    }

}

export default ProductPage;