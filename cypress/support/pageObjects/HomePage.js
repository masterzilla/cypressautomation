class HomePage
{
    getEditName()
    {
        return cy.get(':nth-child(1) > .form-control')
    }

    getEditEmail()
    {
        return cy.get(':nth-child(2) > .form-control')
    }

    getEditPwd()
    {
        return cy.get('#exampleInputPassword1')
    }

    getCheckLoveIce()
    {
        return cy.get('#exampleCheck1')
    }

    getTowWayDatainding()
    {
        return cy.get(':nth-child(1) > .form-control')
    }

    getGender()
    {
        return cy.get('#exampleFormControlSelect1')
    }
    
    getRadioEmploymentStatus()
    {
        return cy.get('#inlineRadio1')
    }

    getEntrepreneur()
    {
        return cy.get('#inlineRadio3')
    }

    getSubmit()
    {
        return cy.get("[type='submit']")
    }

    getShopTab()
    {
        return cy.get(':nth-child(2) > .nav-link')
    }
    
}

export default HomePage;