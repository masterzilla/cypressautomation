class CheckOutPage 
{
    getCheckOutButton()
    {
        return cy.get(':nth-child(5) > :nth-child(5) > .btn')
    }

    getRemoveButton()
    {
        return cy.get(':nth-child(1) > :nth-child(5) > .btn')
    }

    getReturnPageShop()
    {
        return cy.get(':nth-child(2) > .nav-link')
    }

    getCheckOutButtonAgain()
    {
        return cy.get(':nth-child(3) > :nth-child(5) > .btn')
    }
    
    getContinueShoppingButton()
    {
        return cy.get(':nth-child(4) > .btn')
    }

}

export default CheckOutPage;